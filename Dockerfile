FROM httpd:2.4

LABEL maintainer="squiddog@squiddog.org"
ARG MODPERLURL="https://mirrors.gigenet.com/apache/perl/mod_perl-2.0.11.tar.gz"
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt install -y --no-install-recommends curl make gcc libperl-dev perl
RUN cd /tmp && curl -kL ${MODPERLURL} | tar -xzv \
    && cd mod_perl-2.0.11 && perl Makefile.PL MP_APXS=/usr/local/apache2/bin/apxs \
    && make && make install && cd /root && rm -rf /tmp/* 
#RUN ln -s /usr/share/perl /usr/local/share/ \
#    && curl -Lk https://cpanmin.us | perl - App::cpanminus && cpanm SCGI
RUN apt remove -y curl make gcc libperl-dev
RUN apt autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/